classdef TDSim < handle
    properties
        % ↓Properties that are connected to the system and emualted
        device_name;
        DeviceSF;
        TargetValue;
        SysMode;
        nPeriod;
        nPulses;
        nDur_A;
        nDur_B;
        Amp_A;
        read_index;
        stim_buff;
        not_done;
        read_durr;
        read_buff;
        current_time;
        dur;
        trig_stim;
        last_stim_time;
        start_time;
        stim_buff_tild;
        % ↓Fake Properties - Simulated properties from the lab
        n_buff_points;
        rat_model;
    end
    methods
        function obj = TDSim()
           obj.device_name = 'RZ2_1';
           obj.start_time = posixtime(datetime('now'));
           obj.not_done = 0;
        end
        function [name] = get.device_name(obj)
           name = obj.device_name;
        end
        function [FS] = get.DeviceSF(obj)
            FS = 2.4414e04;
        end
        function [obj] = set.TargetValue(obj, value)
            obj.TargetValue = value;
        end
        function [obj] = set.SysMode(obj, value)
            obj.SysMode = value;
        end
        function [name] = GetDeviceName(obj,val)
           name = obj.device_name;
        end
        function [FS] = GetDeviceSF(obj, random)
            FS = obj.DeviceSF;
        end
        function [obj] = set.rat_model(obj, input)
            obj.rat_model = input;
        end
        %Sets the Target Value for the required pulses    
        function [obj] = SetTargetVal(obj, val, num1)
           switch val
               case [obj.device_name '.nperiod']
                   obj.nperiod = val;
               case [obj.device_name '.nPulses']
                   obj.nPulses = val;
               case [obj.device_name '.nDur-A']
                   obj.nDur_A= val;
               case [obj.device_name '.nDur-B']
                   obj.nDur_B = val;
               case [obj.device_name '.Amp-A']
                   obj.Amp_A = val;
               case [obj.device_name '.read_durr']
                   obj.read_durr = val;
               case [obj.device_name '.dur']
                   obj.dur = val;
               case [obj.device_name '.trig_stim']
                   obj.trig_stim = val;
                   if val == 1
                       obj.last_stim_time = posixtime(datetime('now')) - obj.start_time;
                   end
               otherwise
                   warning('Unexpected Input');
           end
        end
        function [obj] = SetSysMode(obj, val)
           obj.SysMode = val; 
        end
        %Takes the rat model object and loads it as a gp-model object.
        %Ouput is a repeat 16x1 matrix that samples the rat model object.
        function [output] = ReadTargetVEX(obj, inputString, val1, val2, str1, str2)
            obj.rat_model = load(fullfile('C:\Users\Zarif Rahman\Documents\CodeBaseDev\Rat_Model','R1_gamma_3D.mat'));
            gp_object = obj.rat_model.gp_model;
            switch inputString
                case [obj.device_name '.read_index']
                    obj.read_index = 5;
                    output = obj.read_index;
                case [obj.device_name '.not_done']
                    output = obj.not_done;
                case [obj.device_name '.read_buff']
                    obj.n_buff_points = val2;
                    t = 1/obj.DeviceSF:1/obj.DeviceSF:.5;
                    frequency_from_vector = obj.Stim_Decode('standard');
                    output = repmat(gp_object.sample(obj.Stim_Decode('standard')')*cos(frequency_from_vector(2)*2*pi*t), 16, 1);
                      
                    
                case [obj.device_name '.current_time']
                    output = (posixtime(datetime('now')) - obj.start_time)*obj.DeviceSF;
                case [obj.device_name '.stim_buff~']
                    output = obj.stim_buff_tild;
                case [obj.device_name '.stim_buff']
                    output = obj.stim_buff;
                case [obj.device_name '.last_stim_time']
                    output = obj.last_stim_time;
                otherwise
                    warning('Unexpected Input');
                    output = -1;
            end
        end
        
%         Stim Decode function that sets the values for the differences in
%         graph plot. The first output gets the max value from the
%         stim_buff. The second output gets the frequency of the Device
%         Frequency alongside the top-right most portion of the square
%         wave. The third output gets pulse width.
        function output = Stim_Decode(obj, input)
            
            read = obj.stim_buff;
            obj.last_stim_time = (posixtime(datetime('now')) - obj.start_time)*obj.DeviceSF;
            switch input
                case 'standard'
                    if isempty(obj.stim_buff)
                        output = zeros(3,1);
                    else
                        output = zeros(3,1);
                        [X0, Y0] = max(read);
                        output(1) = X0;
                        diffvec = diff(read);
                        [X1, Y1] = max(diffvec);
                        [X2, Y2] = min(diffvec);
                        pulse_width = ((X1-X2)/obj.DeviceSF)*1000;
                        output(3) = pulse_width;
                        vec = find(diffvec>0, 2);
                        frequency = 1/((vec(2)-vec(1))/obj.DeviceSF);
                        output(2) = frequency;
                    end
            end
        end
        % ↓ Writing to stim device
        function obj = WriteTargetVEX(obj, inputString, val1, val2, val3)
            switch inputString
                case [obj.device_name '.stim_buff']
                    obj.stim_buff = val3;
                case [obj.device_name '.stim_buff~']
                    obj.stim_buff_tild = val3;
                    
                otherwise
                    warning('Unexpected Input');
            end
        end
    end
end
